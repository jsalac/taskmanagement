@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<button class="btn btn-primary form-control" onclick="addNew()">Add Task</button>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-12">
				<table class="table table-stripe" id="task-list">
					<thead>
						<tr>
							<td>Id</td>
							<td>Task</td>
							<td>Created by</td>
							<td>Assigned by</td>
							<td>Assigned to</td>
							<td class="text-center">Status</td>
							<td class="text-center">Action</td>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>

		</div>
	</div>
	

@include('task.new-task')
@include('task.edit-task')


@endsection()

@section('page-script')
	<script src="{{asset('js/user/user.js')}} "></script>
	<script src="{{asset('js/task/task.js')}} "></script>
	<script src="{{asset('js/task/new.js')}} "></script>	
	<script src="{{asset('js/task/edit.js')}} "></script>
@endsection()