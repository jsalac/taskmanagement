

<div class="modal fade" id="editTaskModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="editTaskForm">
                <div class="modal-header">
                    <p class="modal-title">Edit Task</p>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
                </div>

                <div class="modal-body">
                    <input type="hidden" id="edit_id" >
                    <input type="hidden" name="assigned_by" value="{{Session::get('user_id')}}">
                    <div class="form-group">
                        <label for="">Task</label>
                        <textarea type="text" id="edit_task" name="task" class="form-control" placeholder="Task" required autofocus rows="4" cols="50"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="">Assigned to</label>
                        <select class="form-control user-selection" name="assigned_to" id="edit_assigned_to"></select>
                    </div>
                </div>

                <div class="modal-footer">
    
                    <button type="button" class="btn btn-basic offset-md-7" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary col-sm-3 ">Save</button>        
                   
                </div>
            </form>
        </div>
    </div>
</div>


