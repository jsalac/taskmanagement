<?php

use Illuminate\Database\Seeder;
use App\Model\TaskStatus;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $data = ['open','close','on-hold'];
        foreach($data as $stat){
        	 $status = new TaskStatus;
        	 $status->status = $stat;
        	 $status->save();
        }
       


    }
}
