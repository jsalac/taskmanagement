<?php

use Illuminate\Database\Seeder;
use App\Model\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $user = User::where('username','sa')->first();
        if(empty($user)){
        	$user = new User;
        	$user->name = 'Joseph Salac';
        	$user->username = 'sa';
        	$user->password = bcrypt('developer');
        	$user->save();
        };
        

    }
}
