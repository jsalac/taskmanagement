
class Popup {

	static errorMsg(title,msg){
		swal({
			icon:"error",
			title:title,
			text:msg
		});
	}


	static infoMsg(title,msg){
		swal({
			icon:"info",
			title:title,
			text:msg
		});
	}


	static successMsg(title,msg){
		swal({
			icon:"success",
			title:title,
			text:msg    
		});
	}


	static warningMsg(title,msg){
		swal({
			icon:"warning",
			title:title,
			text:msg
		});
	}


	static askMsg(title,msg){
		return swal({
				icon:"warning",
				title:title,
				text:msg,
				buttons: true,
			dangerMode: true
			});
	}

	

}