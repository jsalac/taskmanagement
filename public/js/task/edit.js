

	$(function(){

		$("#editTaskForm").submit(function(e){
			e.preventDefault();
		    var formData = new FormData($('#editTaskForm')[0])
		    var data = {};
		    
		    formData.forEach(function(value, key){
		        data[key] = value;
		    });


		    console.log(data);
		    let id = $("#edit_id").val();
		    http.API("POST",'/api/tasks/update/'+id +'?_method=PATCH',data)
			 .then(function(response){
			    	console.log(response);
			    	Popup.successMsg("Save",response.message);
			    	getList();
			    	$("#editTaskModal").modal("hide");
		    })
		    .catch(function(error){
			    console.log(error);
			});

		});
	})

