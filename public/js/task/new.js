function addNew(){

			$("#newTaskForm")[0].reset();
			$("#new-task-modal").modal("show");
		}


		$(function(){

			$("#newTaskForm").submit(function(e){
				e.preventDefault();
			    var formData = new FormData($('#newTaskForm')[0])
			    var data = {};
			    
			    formData.forEach(function(value, key){
			        data[key] = value;
			    });

			    http.API("POST",'/api/tasks',data)
				 .then(function(response){
				    	console.log(response);
				    	Popup.successMsg("Save",response.message);
				    	this.getList();
				    	$("#new-task-modal").show("hide");
			    })
			    .catch(function(error){
				    console.log(error);
				});

			});
		})


// function addNew(){

// 			$("#newTaskForm")[0].reset();
// 			$("#new-task-modal").modal("show");
// 		}


// 		$(function(){

// 			$("#newTaskForm").submit(function(e){
// 				e.preventDefault();
// 			    var formData = new FormData($('#newTaskForm')[0])
// 			    var data = {};
			    
// 			    formData.forEach(function(value, key){
// 			        data[key] = value;
// 			    });

// 			    http.API("POST",'/api/tasks',data)
// 				 .then(function(response){
// 				    	console.log(response);
// 				    	Popup.success("Save","New task saved!");
// 				    	this.getList();
// 			    })
// 			    .catch(function(error){
// 				    console.log(error);
// 				});

// 			});
// 		})

