		

		$(document).ready(function(){
			loadUserToSelection();
			getList();
			
		});



		function loadUserToSelection(){
			let User = new user;
			User.loadToSelection('.user-selection');
		}



		function getList(){

			http.API('get','/api/tasks',{'Authorization':'Bearer ' + ACCESS_TOKEN,'Content-Type':'application/json'})
			.then(function(response){
				console.log(response);
				displayList(response.data);
			})
			.catch(function(error){
				console.log(error);
			});
		}


		
		

		function displayList($data){

			$("#task-list tbody tr").detach();
			$list = $data;
			

				
			$list.map(function(task){
				$("#task-list tbody").append(
				`<tr>
					<td> ` + task.id + ` </td>
					<td> ` + task.task + ` </td>
					<td> ` + task.creator + ` </td>
					<td> ` + task.assignedby + ` </td>
					<td> ` + task.assignedto + ` </td>
					<td> ` + task.status + ` </td>
					<td class="text-center"> 
						<i onclick="editTask(${task.id})" class="fas fa-edit text-primary" style='margin:3px'> </i> 
						<i onclick="closeTask(${task.id})" class="fas fa-user-check text-success" style='margin:3px'></i>
						<i onclick="deleteTask(${task.id})" class="text-danger fas fa-trash" style='margin:3px'></i> </td>
				</tr>
				`);
			});


		}


		function editTask($id){

				$("#newTaskForm")[0].reset();
				http.API("GET",'/api/tasks/'+$id)
				.then(function(response){
					console.log(response.data);
					let data = response.data;
					$("#edit_task").val(data.task);
					$("#edit_assigned_to").val(data.assigned_to);
					$("#edit_id").val(data.id);
					$("#editTaskModal").modal("show");
				})
				.catch(function(error){
					Popup.errorMsg("Alert","Sorry failed to get task - please try again");
				});

				
			}



			



		function closeTask($id){
			Popup.askMsg("Confirmation","Close this task?")
			.then((ans)=>{
				if(ans){

					http.API("POST",'/api/tasks/close/'+$id+'?_method=PATCH')
					.then(function(response){
						console.log(response);
						getList();
						Popup.successMsg("Success",response.message);
					})
					.catch(function(error){
						Popup.errorMsg("Sorry Errors occured - please try again");
					})
				}

			});
			
		}



		function deleteTask($id){
			
			Popup.askMsg("Confirmation","Are you sure you want to delete this task? -this is permanent delete")
			.then((answer)=>{
				if(answer){
					http.API("POST","/api/tasks/delete/"+$id+'?_method=DELETE')
					.then(function(response){
						if(response.success){
							Popup.successMsg("Deleted","Task Successfully deleted!");
							getList();
						}
					})
					.catch(function(error){
						Popup.errorMsg("Error occured!");
					})
				}
			})

		}