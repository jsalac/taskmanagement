class HttpRequest { 

	API(method,url,requestHeader ={},requestBody =[]) {
		 return $.ajax({
                url:url,
                method:method,
                data:requestBody,
                async:false,
                beforeSend:function(request){
                    // for(var key in requestHeaders){
                        request.setRequestHeader(requestHeader);
                    // }
                },
                error:function(xhr, error, thrown){
                    console.log(xhr.responseJSON);
                },
                complete:function(data){
                   console.log(data);
                }
            });
	}

}