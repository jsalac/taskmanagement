class HttpRequest { 

	API(method,url,requestBody=[]) {
		 return $.ajax({
                url:url,
                method:method,
                data:requestBody,
                beforeSend:function(request){
                    request.setRequestHeader('Authorization','Bearer ' + ACCESS_TOKEN);
                }
            });
	}

}