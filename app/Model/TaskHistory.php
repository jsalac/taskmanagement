<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TaskHistory extends Model
{
    //
    protected $fillable = ['task_id','remarks'];
}
