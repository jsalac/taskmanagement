<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TaskStatus extends Model
{
    //
    protected $fillable = ['status'];
}
