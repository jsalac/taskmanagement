<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    //
    protected $fillable = ['task','task_status_id','user_id'];

    public function createdBy(){
    	return $this->belongsTo('App\Model\User');
    }


    public function assignedBy(){
    	return $this->belongsTo('App\Model\User','assigned_by');
    }


    public function assignedTo(){
    	return $this->belongsTo('App\Model\User','assigned_to');
    }


    public function history(){
    	return $this->hasMany('App\Model\TaskHistory');
    }




}
