<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
     public function register()
    {
        //
        $this->registerRepo();
    }

    private function registerRepo(){
        // $this->app->bind('App\Repositories\BaseInterface','App\Repositories\BaseRepository');
        // $this->app->bind('App\Repositories\UserInterface','App\Repositories\UserRepository');
        $this->app->bind('App\Repositories\TaskInterface','App\Repositories\TaskRepository');
        // $this->app->bind('App\Repositories\TStatusInterface','App\Repositories\TStatusRepository');
        // $this->app->bind('App\Repositories\THistoryInterface','App\Repositories\THistoryRepository');
    }
        
}
