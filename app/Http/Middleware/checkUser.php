<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Session;


class checkUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
      
        if($request->route()->uri=='login'){
            if(Session::has('user')){
                 return redirect('/');
            }else{
                return $next($request);
            }
        }   
        if(Session::has('user')){
          return $next($request);
        }
        return redirect('/login')->with('Erros','Please Log in');


    }
}
