<?php

namespace App\Http\Controllers\Auth;

use App\Model\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use App\Repositories\UserInterface;
use JWTFactory;
use JWTAuth;
use Response;
use Session;
use Auth;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    // protected $user;

    // public function __construct(UserInterface $interface){
    //     $this->user = $interface;
    // }

    use RegistersUsers;

    public function register(Request $request)
    {   
        $validator = Validator::make($request->all(), [
            'username'  => 'required|max:255|unique:users,username',
            'name'      => 'required',
            'password'  => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $data = [
            'name'      => $request->get('name'),
            'username'  => $request->get('username'),
            'password'  => bcrypt($request->get('password')),
        ];

        $user = User::create($data);
        $token = JWTAuth::fromUser($user);
        Session::put('user',$data);
        Session::put('access-token',$token); 
        return Response::json(compact('token'));
    }


    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    // protected function validator(array $data)
    // {
       
    //     return Validator::make($data, [
    //         'name' => 'required|string|max:255',
    //         'username' => 'required|string|unique:users',
    //         'password' => 'required|string|min:6|confirmed',
    //     ]);
    // }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    // protected function create(array $data)
    // {
    //     return User::create([
    //         'name' => $data['name'],
    //         'username' => $data['username'],
    //         'password' => Hash::make($data['password']),
    //     ]);
    // }
}
