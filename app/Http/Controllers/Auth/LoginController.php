<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use JWTFactory;
use JWTAuth;
use Validator;
use Auth;
use Session;
use Response;
use App\Model\User;

class LoginController extends Controller
{
    
    
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username()
    {
        return 'username';
    }


    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required|string|max:255',
            'password'=> 'required'
        ]);

        if ($validator->fails()) {
            // return response()->json($validator->errors());
            return redirect('/login')->with('error', $validator->errors());
        }
        $credentials = $request->only('username', 'password');

        try {
            if (! $token = JWTAuth::attempt($credentials)) {

                // return response()->json(['error' => 'invalid_credentials'], 401);
                return redirect('/login')->with('error', 'Invalid credentials');
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        $user = User::where('username',$request->username)->first();
        Auth::attempt($credentials);
        $token = JWTAuth::fromUser($user);
        Session::put('access-token',$token );
        Session::put('user_id',$user->id);
        return redirect('/task');

    }


    public function logout(){
        SESSION::flush();
        return redirect('/login')  ;
        // return response()->json(compact('token'));
    }
}
