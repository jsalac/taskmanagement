<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use App\Repositories\TaskInterface;
// use App\Repositories\TStatusInterface;
// use App\Repositories\THistoryInterface;

use App\Model\Task;
use App\Model\TaskHistory;
use App\Model\TaskStatus;
use App\Model\User;


class TaskController extends Controller
{
    //

    protected $task;

    // public function __construct(TaskInterface $task){
    //   $this->task = $task;
    // }


    public function getAll(){
    	$tasks = Task::selectRaw('a.name as creator,b.name assignedby ,c.name assignedto,ts.status, tasks.id,tasks.task,tasks.user_id,assigned_by,assigned_to')
                        ->join('users as a','a.id','user_id')
                        ->join('users as b','b.id','assigned_by')
                        ->join('users as c','c.id','assigned_to')
                        ->join('task_statuses as ts','ts.id','task_status_id')
                        ->get();
                        

      return response()->json([     
          'success'=>true,
          'data'=>$tasks
      ],200);
    }


    public function get($id){
      $tasks = Task::selectRaw('a.name as creator,b.name assignedby ,c.name assignedto,ts.status, tasks.id,tasks.task,tasks.user_id,assigned_by,assigned_to')
                        ->join('users as a','a.id','user_id')
                        ->join('users as b','b.id','assigned_by')
                        ->join('users as c','c.id','assigned_to')
                        ->join('task_statuses as ts','ts.id','task_status_id')
                        ->where('tasks.id',$id)->first();
                        

      return response()->json([     
          'success'=>true,
          'data'=>$tasks
      ],200);
    }




    public function create(Request $request) {

    	$status = TaskStatus::where('status','open')->first();

    	if(!empty($status)){
        $task = new Task;
    		$task->task 		=	$request->task;
	    	$task->user_id	=	$request->user_id;
	    	$task->assigned_by	=	 $request->assigned_by;
	    	$task->assigned_to	=	 $request->assigned_to;
	    	$task->task_status_id		= $status->id;
	    	$success = $task->save();

	    	if($success){
	    		return response()->json([
	    			'success'=>true,
	    			'message'=>'New task save',
            'data'=>$task
	    		],201);
	    	}

    	}else{

    		return response()->json([
    			'success'=>false,
    			'message'=>'Errors Occured please check your data'
    		],409);

    	}

    	

    }



  	public function update(Request $request,$id){

  
	  		$task = Task::find($id);
        $task->task = $request->task;
        $task->assigned_to = $request->assigned_to;
        $success = $task->save();
        
	  		if($success){
	  			return response()->json([
		    		'success'	  =>	true,
		    		'message' 	=>	'Task Successfully updated',
		    		'data'		  =>	$task
		    	],200);
	  		}else{
          return response()->json([
            'success'   =>  true,
            'message'   =>  'Task does not exist',
            'data'      =>  $task
          ],404);
        }
  	
  		
  	}





    public function closeTask($id){

      $task = Task::find($id);
      $status = TaskStatus::where('status','close')->first();
      if($task){
        // delete task
        $task->task_status_id=$status->id;
        $task->save();
        // return response toclient
        return response()->json([
            'success' =>  true,
            'message'   =>  'Task Successfully closed',
            'data'    =>  $task
          ],200);

      }
    }





  	public function remove($id){

      $task = Task::find($id);

      if($task){
        // delete task
        $task->delete();
        // return response toclient
        return response()->json([
            'success' =>  true,
            'message'   =>  'Task Successfully updated',
            'data'    =>  $task
          ],200);

      }else{

          return response()->json([
          'success'=>false,
          'message'=>'Task does not exist or might be deleted by other user.'
        ],404);

      }
        
    }


  	public function logHistory($logs){
  		return $this->history->create($logs);
  	}

}
