<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\UserInterface;
use App\Model\User;
use App\Model\TaskStatus;

class UserController extends Controller
{
    //
	// protected $user;
 //    protected $status;

 //    public function __construct(User $model,TaskStatus $status) {
 //    	$this->user = $model;
 //        $this->status = $status;

 //    }


    public function getAll(){
        $users =  User::all();
        return response()->json([
            'success'=>true,
            'data'=> $users
        ],200);
    }


    public function getAllTask($id) {
    	$task =User::find($id)->myTasks();
        return $response->json([
            'success'=>true,
            'data'=> $task
        ],200);
    }


    public function getOpenTask() {

        $TStatus = $this->status->where('status','open');
        $task = User::find($id)->myTasks()->where('status_id',$TStatus->id)->get();

        return $response->json([
            'success'=>true,
            'data'=> $task
        ],200);
    }


    public function getClosedTask() {
        
        $TStatus = $this->status->where('status','close');
        $task = User::find($id)->myTasks()->where('status_id',$TStatus->id)->get();

        return $response->json([
            'success'=>true,
            'data'=> $task
        ],200);
    }
}
