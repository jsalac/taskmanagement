<?

namespace App\Repositories;

Interface BaseInterface {

	public function create($data);
	public function update($data);
	public function remove($id);
	public function find($id);
	public function all();
	public function model();
}
