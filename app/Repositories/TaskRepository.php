<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Repositories\TaskInterface;
use App\Model\Task;

class TaskRepository extends BaseRepository implements TaskInterface {

	protected  $model;

    public function __construct(Task $model){
        $this->model = $model;
    }

}