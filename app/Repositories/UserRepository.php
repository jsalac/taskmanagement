<?php

namespace App\Repositories;
use App\Repositories\UserInterface;
use App\Repositories\BaseRepository;
use App\Model\User;

class UserRepository extends BaseRepository implements UserInterface {

	protected  $model;

    public function __construct(User $model){
        $this->model = $model;
    }

}