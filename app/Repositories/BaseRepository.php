<?php

namespace App\Repositories;

use App\Repositories\BaseInterface;

class BaseRepository implements BaseInterface {
    
    public function create($data){
        return $this->model->create($data);
    }

    public function update($data,$id){
        return $this->model->find($id)->update($data);
        
    }


    public function find($id){
        return $this->model->find($id);

    }

    public function all(){
        return $this->model->all();
    }


    public function remove($id){
        return $this->model->destroy($id);
    }


    public function model(){
    	return $this->model;
    }
}

