<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



// Route::get('/tasks','TaskController@getAll');





// Route::middleware('jwt.auth')->prefix('/', function() {
//     Route::get('test',function(){
//     	return 'asss';
//     });
// });

Route::group(['middleware' => 'jwt.auth'], function () {

	// Task
	Route::get('/tasks/{id}', 'TaskController@get');
    Route::get('/tasks', 'TaskController@getAll');
    Route::post('/tasks', 'TaskController@create');
    Route::patch('/tasks/update/{id}', 'TaskController@update');
    Route::patch('/tasks/close/{id}', 'TaskController@closeTask');
    Route::delete('/tasks/delete/{id}', 'TaskController@remove');

    // users
    Route::get('/users','UserController@getAll');
});